class Todo {
    constructor(Todo = {}) {
        this.description = Todo.description || ""
        this.nombre = Todo.nombre || ""
        this.precio = Todo.precio || null
        this.status = Todo.status || false
    }
}




const app = Vue.createApp({
    el: "#formulario",
    data() {
        return {
            newTodo: new Todo(),
            editTodo: new Todo(),
            todos: [new Todo({
                description: "Pepas de oro",
                nombre: "Oro",
                precio: 500000
            })],
            error: false,
            editIndex: -1,

        }

    },

    methods: {
        addTodo() {
            this.error = false;
            if (this.newTodo.description.length > 0 && this.newTodo.nombre.length > 0 && this.newTodo.precio > 0) {
                this.todos.push(this.newTodo);
                this.newTodo = new Todo()
            } else {
                this.error = true;
            }
        },
        setTodo(index) {
            this.editIndex = index;
            this.editTodo.description = this.todos[index].description;
            this.editTodo.nombre = this.todos[index].nombre;
            this.editTodo.precio = this.todos[index].precio;

        },
        saveTodo(index) {
            this.todos[index].description = this.editTodo.description;
            this.todos[index].nombre = this.editTodo.nombre;
            this.todos[index].precio = this.editTodo.precio;
            this.editIndex = -1;
        },
        deleteTodo(index) {
            const confirm = window.confirm("Estas seguro de querrer eliminarlo")
            this.todos.splice(index, 1);
        },
        cancel() {
            this.editIndex = -1
        }


    }

});
app.mount("#app")